package com.company;

import java.util.HashMap;
import java.util.HashSet;

public class Contact {
    HashMap<String, HashSet<String>> hm;

    public Contact() {
        this.hm = new HashMap<>();
    }

    public void addContact(String surname, String phone) {
        HashSet<String> hs = hm.getOrDefault(surname, new HashSet<>());
        hs.add(phone);
        hm.put(surname, hs);
    }

    public void findPhoneOfContact(String initSurname) {
        if (hm.containsKey(initSurname)) {
            System.out.println("Контакт " + initSurname + " найден. Номера телефонов:" + hm.get(initSurname));
        } else {
            System.out.println("Контакт не найден!");
        }
    }
}
