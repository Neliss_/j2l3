package com.company;

import java.util.*;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {

        System.out.println("Список слов:");
        printArray();
        System.out.println("Сколько раз встречается каждое слово?");
        initNumsOfRepetitions();
        System.out.println("Список уникальных слов в алфавитном порядке:");
        initUniqueWords();

        System.out.println();
        System.out.println("Телефонная книга");

        Contact phoneBook = new Contact();
        phoneBook.addContact("Ivanov", "89171001010");
        phoneBook.addContact("Petrov", "89172002020");
        phoneBook.addContact("Sidorov", "89173003030");
        phoneBook.addContact("Ivanov", "89174004040");

        System.out.println(phoneBook.hm);
        phoneBook.findPhoneOfContact("Ivanov");


    }

    static String[] arrayOfFruits = {"apple", "pineapple", "banana", "peach", "mango", "apple", "banana", "pineapple", "apple", "grape", "mango", "apple", "banana", "banana", "mango"};

    static LinkedList<String> fruit = new LinkedList<>();

    static void printArray() {
        for (String o :
                arrayOfFruits) {
            fruit.add(o);
        }
        System.out.println(fruit);
    }

    static Map<String, Integer> hm = new HashMap<>();

    static void initNumsOfRepetitions() {
        for (int i = 0; i < fruit.size(); i++) {
            Integer counter = hm.get(fruit.get(i));
            hm.put(String.valueOf(fruit.get(i)), counter == null ? 1 : counter + 1);
        }
        System.out.println(hm);
    }

    static void initUniqueWords() {
        TreeMap<String, Integer> tm = new TreeMap<>(hm);
        Set<String> keys1 = tm.keySet();
        System.out.println(keys1);
    }
}
